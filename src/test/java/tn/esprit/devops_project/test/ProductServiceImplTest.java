package tn.esprit.devops_project.test;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tn.esprit.devops_project.entities.Product;
import tn.esprit.devops_project.repositories.ProductRepository;
import tn.esprit.devops_project.services.Iservices.IProductService;
import tn.esprit.devops_project.services.ProductServiceImpl;

import java.util.List;


@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProductServiceImplTest {

    private ProductServiceImpl productService;
    private ProductRepository productRepository;
    @Autowired
    ProductServiceImpl ps;

    @Test
    @Order(1)
    public void testRetrieveAllProducts () {
            List<Product> listProduits = ps.retreiveAllProduct();
            Assertions.assertEquals(0, listProduits.size());
        }


}