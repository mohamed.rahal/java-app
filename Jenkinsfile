pipeline {
    agent any

    environment {
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "localhost:8081"
        NEXUS_REPOSITORY = "java-app"
        NEXUS_CREDENTIAL_ID = "NEXUS_CRED"
    }

    stages {
        stage('Unit Tests') {
            steps {
                script {
                    sh 'mvn clean test'
                }
            }
        }

        stage('Build Backend') {
            steps {
                script {
                   
                    sh 'mvn package -DskipTests'
                }
            }
        } 
       
     stage('Code Coverage Analysis') {
    steps {
        script {
            sh 'mvn test jacoco:report'
            junit(testResults: 'target/surefire-reports/*.xml', allowEmptyResults: true)
            archiveArtifacts allowEmptyArchive: true, artifacts: 'target/site/jacoco/index.html'
        }
    }
    }

        stage("Publish to Nexus Repository Manager") {
            steps {
                script {
                    pom = readMavenPom file: "pom.xml"
                    filesByGlob = findFiles(glob: "target/*.${pom.packaging}")
                    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
                    artifactPath = filesByGlob[0].path
                    artifactExists = fileExists artifactPath
                    if (artifactExists) {
                        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}"
                        nexusArtifactUploader(
                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: pom.groupId,
                            version: pom.version,
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: artifactPath,
                                type: pom.packaging],
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: "pom.xml",
                                type: "pom"]
                            ]
                        )
                    } else {
                        error "*** File: ${artifactPath}, could not be found"
                    }
                }
            }
        } 

         stage("build image & Push ") {
            steps {
                script {
                   echo "building the docker image for the java app..."
                    withCredentials([usernamePassword(credentialsId: 'ca7a83f9-e987-4006-9576-2f816e2530eb', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                    sh 'docker build -t mohamedrahal/java-app:latest .'
                    sh "echo $PASS | docker login -u $USER --password-stdin"
                    sh 'docker push mohamedrahal/java-app:latest'
                 }
                }
            }
        }

                stage('Build Frontend image and Push') {
             steps {
                script {
                    git branch: 'main', credentialsId: 'e577eb06-1e1c-4ed0-8dee-cfdc6ff23ed1', url: 'https://gitlab.com/mohamed.rahal/frontproject'
                    sh 'npm install --force'
                    sh 'npm run build' 
                    echo "building the docker image for the java app..."
                    withCredentials([usernamePassword(credentialsId: 'ca7a83f9-e987-4006-9576-2f816e2530eb', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                    sh 'docker build -t mohamedrahal/angular-app:latest .'
                    sh "echo $PASS | docker login -u $USER --password-stdin"
                    sh 'docker push mohamedrahal/angular-app:latest'

                }
            }
        }    
    
    }

        stage('Docker Compose') {
            steps {
                script {
                    sh 'docker-compose up -d'
                }
            }
        }

    }
}

