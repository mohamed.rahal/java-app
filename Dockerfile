FROM openjdk:11
ADD target/DevOps_Project-1.0.jar DevOps_Project-1.0.jar
EXPOSE 8282
ENTRYPOINT ["java","-jar","/DevOps_Project-1.0.jar"]